<?xml version="1.0" encoding="UTF-8" ?>
<Package name="dialog_counting" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions/>
    <Dialogs>
        <Dialog name="dlg_counting" src="dlg_counting/dlg_counting.dlg" />
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="dialog_counting_service" src="scripts/dialog_counting_service.py" />
        <File name="__init__" src="scripts/stk/__init__.py" />
        <File name="events" src="scripts/stk/events.py" />
        <File name="logging" src="scripts/stk/logging.py" />
        <File name="runner" src="scripts/stk/runner.py" />
        <File name="services" src="scripts/stk/services.py" />
    </Resources>
    <Topics>
        <Topic name="dlg_counting_enu" src="dlg_counting/dlg_counting_enu.top" topicName="dlg_counting" language="en_US" />
        <Topic name="dlg_counting_czc" src="dlg_counting/dlg_counting_czc.top" topicName="dlg_counting" language="cs_CZ" />
    </Topics>
    <IgnoredPaths />
</Package>

# Counting Dialog

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is a dialog application, in both Czech and English, that enables the robot to do simple computations.

Examples of supported inputs:
* "Kolik je jedna plus dvě?"/ "How much is one plus two?"
* "Kolik je dva krát čtyři?" / "How much is two times four?"

Current version supports numbers up to 999 with four operations: plus,
minus, times and divided-by.

## Installation

* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot


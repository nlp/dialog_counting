#!/usr/bin/python
# -*- coding: utf-8 -*-
#
"""
NAOqi service for counting
"""

__version__ = "0.0.1"

__copyright__ = "Copyright (C) 2019, NLP Centre, FI MU"
__author__ = 'Ales Horak'
__email__ = 'hales@fi.muni.cz'


import qi

import stk.runner
import stk.events
import stk.services
import stk.logging

import math
import re
import sys

DEBUG_CONVERT = False
#DEBUG_CONVERT = True
NUMBER_WORDS = {
    # enu
    'hundred': 100,
    'hundreds': 100,
    # czc
    'nulou': 0,
    'jeden': 1,
    'dvě': 2,
    'dvěma': 2,
    'dvěmi': 2,
    'třema': 3,
    'třemi': 3,
    'čtyřma': 4,
    'čtyřmi': 4,
    'pěti': 5,
    'šesti': 6,
    'sedmi': 7,
    'osmi': 8,
    'osum': 8,
    'devíti': 9,
    'deseti': 10,
    'jedenácti': 11,
    'jedním': 11,
    'jedno': 11,
    'jednou': 11,
    'dvanácti': 12,
    'třinácti': 13,
    'čtrnácti': 14,
    'štrnáct': 14,
    'štrnácti': 14,
    'patnácti': 15,
    'šestnácti': 16,
    'sedmnácti': 17,
    'sedumnácti': 17,
    'osmnácti': 18,
    'osumnáct': 18,
    'osumnácti': 18,
    'devatenácti': 19,
    'dvaceti': 20,
    'dvacíti': 20,
    'třiceti': 30,
    'čtyřiceti': 40,
    'padesáti': 50,
    'šedesáti': 60,
    'sedmdesáti': 70,
    'osmdesáti': 80,
    'osumdesáti': 80,
    'devadesáti': 90,
    'set': 100,
    'sta': 100,
    'stě': 100,
    'stem': 100,
    'sto': 100,
    'sty': 100,
    'dvěsta': 200,
    'dvěstě': 200,
    'pěcet': 500,
    'šescet': 600,
    'devěcet': 900,
}

OPERATOR_WORDS = {
    # enu
    'plus': '+',
    'minus': '-',
    'times': '*',
    'divided by': '/',
    # czc
    #'plus': '+',
    'mínus': '-',
    'krát': '*',
    'děleno': '/',
}

class ALDialogCounting(object):
    "NAOqi service example (set/get on a simple value)."
    APP_ID = "com.aldebaran.ALDialogCounting"
    def __init__(self, qiapp):
        # generic activity boilerplate
        if DEBUG_CONVERT:
            import logging
            self.logger = logging.getLogger()
            self.logger.setLevel(logging.DEBUG)
            handler = logging.StreamHandler(sys.stdout)
            handler.setLevel(logging.DEBUG)
            self.logger.addHandler(handler)
        else:
            self.qiapp = qiapp
            self.events = stk.events.EventHelper(qiapp.session)
            self.s = stk.services.ServiceCache(qiapp.session)
            self.logger = stk.logging.get_logger(qiapp.session, self.APP_ID)

    @qi.bind(returnType=qi.String, paramsType=[qi.String])
    def compute(self, command):
        result = 1.0
        command = str(command)
        m = re.match('(.*) (' + '|'.join(OPERATOR_WORDS) + ') (.*)', command)
        if m:
            number1 = self.convert_number(m.group(1))
            operator_word = m.group(2)
            operator = OPERATOR_WORDS[operator_word]
            number2 = self.convert_number(m.group(3))
            try:
                result = str(int(eval(str(number1) + operator + str(number2)))).replace('-','minus')
            except:
                result = 'nan'
            self.logger.info("ALDialogCounting '{}': {} {} {} = {}".format(command, \
                number1, operator, number2, result))
            return ("{} {} {} equals {}".format(int(number1), operator_word, int(number2), result))
        else:
            result = self.convert_number(command)
            self.logger.info("ALDialogCounting '{}': {}".format(command, result))
            return (str(int(result)))

    @qi.nobind
    def convert_number(self, text):
        number_array = []
        for word in text.split(' '):
            if re.match('^\d+$', word):
                number_array.append(int(word))
            else:
                number_array.append(NUMBER_WORDS[word])
        result = 0.0
        prev_number = 0.0
        for number in number_array:
            if prev_number >0 and prev_number < number:
                result = result -prev_number + prev_number * number
            else:
                result = result + number
            prev_number = number
        # qi.logging does note have logger.debug :-(
        #self.logger.info("ALDialogCounting {} = {}".format(number_array, result))
        return (result)

    @qi.nobind
    def on_start(self):
        pass

    @qi.bind(returnType=qi.Void, paramsType=[])
    def stop(self):
        "Stop the service."
        self.logger.info("ALDialogCounting stopped by user request.")
        self.qiapp.stop()

    @qi.nobind
    def on_stop(self):
        "Cleanup (add yours if needed)"
        self.logger.info("ALDialogCounting finished.")
        self.events.clear()

####################
# Setup and Run
####################

if __name__ == "__main__":
    if DEBUG_CONVERT:
        a = ALDialogCounting(None)
        a.compute('2 sto 20 sedmi plus 2')
        a.compute('devěcet dvěmi děleno osumnácti')
        sys.exit(0)
    stk.runner.run_service(ALDialogCounting)

